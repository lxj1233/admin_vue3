🙏🙏尊敬的小主，为了更好的开源，请动一下可爱的小手指，点一下 star 吧~~🙏🙏

# admin vue3 vite javascript


## 简介

使用了当今流行的技术框架： Vue3 + Vite2 + Javascript + Antv UI

## 项目地址

🙏🙏 [项目地址](https://gitee.com/lxj1233/admin_vue3)

🙏🙏 暂无演示地址，请自行 
```
# 克隆项目
git clone https://gitee.com/lxj1233/admin_vue3.git
# 进入目录
cd admin-vue3
# 安装依赖
npm install
# 本地开发 启动项目
npm run dev
# 登录页面随便输入用户名和密码即可登录
```

## 鸣谢

| |
| ---- |
| vue3 |
| vite |
| antv |
| vue-router | 
| vuex |

## 特别说明

项目为个人开发，可能存在诸多bug，可以联系VX：linxinjian1233

欢迎开发者参与和提出意见

项目部分代码参考了大神[清清玄](https://gitee.com/qingqingxuan)的相关项目，在此表示感谢分享🙏🙏🙏🙏
