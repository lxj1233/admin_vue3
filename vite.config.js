import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'

function pathResolve(dir) {
  return resolve(process.cwd(), '.', dir);
}

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@/': pathResolve('src') + '/',
      '~': pathResolve('node_modules') + '/',
      'vue-i18n': pathResolve('node_modules') + '/vue-i18n/dist/vue-i18n.cjs.js'
    }
  },
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
        additionalData: `@import "${pathResolve('src') + '/assets/mytheme.less'}";`
      }
    }
  },
  // server: {
  //   cors: true,
  //   open: true,
  //   proxy: {
  //     '/api': {
  //       target: 'http://192.168.99.223:3000',   //代理接口
  //       changeOrigin: true,
  //       rewrite: (path) => path.replace(/^\/api/, '')
  //     }
  //   }
  // }
})
