// 引入axios
import axios from 'axios'
// 引入 store
import store from '../store'
// 引入 router
import router from '../router'

import { message } from 'ant-design-vue';

import { baseUrl } from '../config'

// 环境切换的请求地址变换
axios.defaults.baseURL = baseUrl

// post请求头的设置
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8'

// 请求拦截器
axios.interceptors.request.use(
  config => {
    const token = store.state.token
    token && (config.headers.Authorization = token);
    return config
  },
  error => {
    return Promise.reject(error.response)
  }
)


// 响应拦截器
axios.interceptors.response.use(
  response => {
    // 如果返回的状态码为200，说明接口请求成功，可以正常拿到数据 
    // 否则的话抛出错误
    if (response.status === 200) {
      return response
    } else {
      throw new Error(response.status.toString())
    }
  },
  // 服务器状态码不是2开头的的情况
  error => {
    if (error.response.status) {
      switch (error.response.status) {
        // 401: 未登录 OR 403 token过期
        case 401:
        case 403:
          // 清除token
          store.commit('del_token')
          // 提示登录失效
          message.error('登录失效，请重新登录')
          // 登陆函数
          router.replace('/login')
          break;
        // 其他错误，直接抛出错误提示
        default:
          message.error('请求失败，未知错误')
          break;
      }
    } else {
      return Promise.reject({ code: -1, msg: '请求失败，未知错误' })
    }
  }
)


export default axios