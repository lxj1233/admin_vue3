/**
 * 菜单相关api
 */
import { get } from './http'

export function getMenus(roleId = 0) {
  return new Promise((resolve, reject) => {
    // get('url', {
    //   roleId
    // }).then(res => {

    // })
    const res = [{
      menuName: '一级菜单',
      menuEnName: 'frist menu',
      menuUrl: '/11111',
      menuAction: ['add', 'edit'],
      icon: 'question-outlined',
      hidden: false,
      cacheable: true,
      affix: false,
      children: [{
        menuName: '二级菜单',
        menuEnName: 'sendce menu',
        menuUrl: '/11111/22222',
        menuAction: ['add', 'edit'],
        icon: 'question-outlined',
        hidden: false,
        cacheable: true,
        affix: false,
      }]
    }]
    resolve(res)
  })
}