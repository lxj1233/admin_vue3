/**
 * 登录相关
 */

import { post } from './http'
import store from '@/store'

export function login(username, password) {
  return new Promise((resolve, reject) => {
    // post('/login').then(res => {
    // })
    const userinfo = {
      username: username,
      token: username,
      roleId: 0
    }
    store.dispatch('user/saveUser', userinfo)
    localStorage.setItem('user_info', JSON.stringify(userinfo))
    resolve()
  })
}