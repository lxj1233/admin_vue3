import path from 'path-browserify'
import test from '@/unit/function/test'
import store from '@/store'
import fun from '@/unit/function'


export function mapTwoLevelRouter(srcRoutes) {
  function addParentRouter(routes, parent, parentPath) {
    routes.forEach((it) => {
      if (!test.url(it.path)) {
        it.path = path.resolve(parentPath, it.path)
      }
      parent.push(it)
      if (it.children && it.children.length > 0) {
        addParentRouter(it.children, parent, it.path)
      }
    })
  }
  if (srcRoutes && srcRoutes.length > 0) {
    const tempRoutes = []
    srcRoutes.forEach((it) => {
      const route = { ...it }
      const parentRoutes = []
      if (route.children && route.children.length > 0) {
        addParentRouter(route.children, parentRoutes, route.path)
      }
      parentRoutes && parentRoutes.length > 0 && (route.children = parentRoutes)
      tempRoutes.push(route)
    })
    return tempRoutes
  }
  return []
}

export function handleRoutes(routes, parentPath = '/') {
  if (!routes) {
    return undefined
  }
  const tempRoutes = []
  routes.forEach((it) => {
    const tempRoute = {
      ...it,
      hidden: it.hidden ? !!it.hidden : false,
      fullPath: test.url(it.path) || path.resolve(parentPath, it.path)
    }
    if (tempRoute.children && tempRoute.children.length > 0) {
      tempRoute.children = handleRoutes(tempRoute.children, tempRoute.fullPath)
    }
    tempRoutes.push({ ...tempRoute })
  })
  return tempRoutes
}


export function addCachedView(route) {
  if (route.name && route.meta && route.meta.cacheable) {
    const humName = fun.toHump(route.name)
    const cachedView = store.getters['cachedView/cachedView']
    if (!cachedView.includes(humName)) {
      cachedView.push(humName)
      store.dispatch('cachedView/changeCachedView', cachedView)
    }
  }
}

export function removeCachedView(route) {
  const humName = fun.toHump(route.name)
  const cachedView = store.getters['cachedView/cachedView']
  const index = cachedView.indexOf(humName)
  if (index !== -1) {
    cachedView.splice(index, 1)
  }
  store.dispatch('cachedView/changeCachedView', cachedView)
}

export function resetCachedView() {
  const visitedView = store.getters['visitedView/visitedView']
  const cachedView = visitedView.filter((it) => {
    return it.name && it.meta && it.meta.cacheable
  }).map((it) => {
    fun.toHump(it.name)
  })
  store.dispatch('cachedView/changeCachedView', cachedView)
}

export function addVisitedView(route) {
  return new Promise((resolve, reject) => {
    const white = ["/404", "/login", "/register"];
    const fullpath = route.fullPath.indexOf('?') > -1 ? route.fullPath.substr(0, route.fullPath.indexOf('?')) : route.fullPath
    if (route.meta?.noShowTabbar || white.includes(fullpath)) {
      reject()
      return false
    }
    const visitedView = store.getters['visitedView/visitedView']
    if (!(visitedView.find((it) => {
      return it.fullPath === fullpath
    }))) {
      visitedView.push(route)
    }
    store.dispatch('visitedView/changeVisitedView', visitedView)
    addCachedView(route)
    resolve(route)
  })
}


export function removeVisitedView(route) {
  return new Promise((resolve) => {
    const visitedView = store.getters['visitedView/visitedView']
    visitedView.splice(visitedView.indexOf(route), 1)
    store.dispatch('visitedView/changeVisitedView', visitedView)
    removeCachedView(route)
    resolve()
  })
}

export function closeAllVisitedView() {
  return new Promise((resolve) => {
    let visitedView = store.getters['visitedView/visitedView']
    visitedView = visitedView.filter((it) => {
      return it.meta && it.meta.affix
    })
    store.dispatch('visitedView/changeVisitedView', visitedView)
    resetCachedView()
    resolve()
  })
}