import store from '@/store'
/**
 * 获取菜单
 * @returns 
 */
export function getMenu() {
  function checkMenuHidden(routes, parentPath = null) {
    var menu = [];
    routes.forEach(item => {
      if (!item.hidden) {
        if (item.children && item.children.length > 0) {
          item.children = checkMenuHidden(item.children, item.path)
          item.children.length === 0 && delete item.children
        }
        delete item.component
        menu.push(item)
      }
    });
    return menu;
  }

  const routes = store.getters["menu/permissionRoutes"]
  return checkMenuHidden(routes);
}


