import {
  createRouter,
  createWebHashHistory
} from 'vue-router'

const layout = () => import('@/components/layout.vue')

import { mapTwoLevelRouter } from '@/unit/router'


export const layourRoutes = [
  {
    path: '/login',
    name: 'Login',
    hidden: true,
    component: () =>
      import('@/views/login/index.vue')
  }, {
    path: '/register',
    name: 'Register',
    hidden: true,
    component: () =>
      import('@/views/login/register.vue')
  }, {
    path: '/',
    redirect: '/dashboard',
    hidden: true
  }, {
    path: '/redirect',
    component: layout,
    hidden: true,
    children: [{
      path: '/redirect/:path(.*)*',
      meta: {
        noShowTabbar: true
      },
      component: () => import('@/views/redirect/index.vue'),
    }]
  }, {
    path: '/personal',
    component: layout,
    hidden: true,
    name: 'personal',
    meta: {
      title: '账号中心',
      enTitle: 'Personal center'
    },
    children: [{
      path: '',
      meta: {
        cacheable: true
      },
      component: () => import('@/views/personal/index.vue'),
    }]
  }, {
    path: '/dashboard',
    name: 'dashboard',
    component: layout,
    meta: {
      title: '主控台',
      enTitle: 'Dashboard',
      icon: 'DashboardOutlined',
      affix: true,
      cacheable: true
    },
    children: [{
      path: '',
      name: 'dashboard_home',
      hidden: true,
      meta: {
        title: '主控台',
        enTitle: 'Dashboard',
        icon: 'DashboardOutlined',
        cacheable: true
      },
      component: () => import('@/views/index/home.vue'),
    }]
  }, {
    path: '/404',
    name: '404',
    hidden: true,
    component: layout,
    children: [{
      path: '',
      name: '404_1',
      component: () => import('@/views/exception/404.vue')
    }]
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes: mapTwoLevelRouter(layourRoutes)
})

export default router