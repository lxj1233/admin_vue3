import { createApp, computed } from 'vue'
import Antd from 'ant-design-vue';
import * as Icons from '@ant-design/icons-vue'
import { createI18n } from 'vue-i18n'
import en from './lang/en'
import zhCN from './lang/zhCN'
import App from './App.vue'
import './assets/mytheme.less'
import store from './store'
import router from './router'
import './router/router'

const app = createApp(App)
app.config.productionTip = false

for (const i in Icons) {
  app.component(i, Icons[i])
}

const i18n = createI18n({
  locale: 'zh-cn',
  messages: {
    'en': en,
    'zh-cn': zhCN
  }
})

app.use(i18n)
app.use(Antd)
app.use(store)
app.use(router)
router.isReady().then(() => {
  app.mount('#app')
})