const zhCN = {
  web: {
    title: '招报云',
    slogn: '快速搭建企业招聘人才库',
    com: '朴山文化创意有限公司',
  },
  welcome: '欢迎',
  fullscreen: {
    exit: "退出全屏",
    do: "全屏显示",
    error: "系统不支持全屏"
  },
  notice: {
    title: "消息中心",
    readed: "标记已读",
  },
  reload: '刷新页面',
  personalCenter: '账号中心',
  logout: {
    index: '退出登录',
    notice: '确定要退出登录？',
    cancelBtnText: '不，点错了',
  },
  login: {
    text: '登录',
    changeMode: '切换登录方式',
    username: '用户名/手机号',
    password: '密码',
    required: '不能为空',
    fillIn: '请填写',
    register: '立即注册',
  },
  model: {
    title: '提示'
  },
  user: {
    defaultNickname: '临时访客',
  }
}

export default zhCN