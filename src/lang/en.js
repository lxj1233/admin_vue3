const en = {
  web: {
    title: 'Joboo Cloud',
    slogn: 'Quickly build enterprise recruitment talent pool',
    com: 'Pushan cultural creativity Co., Ltd',
  },
  welcome: 'Welcome',
  fullscreen: {
    exit: "Out full screen",
    do: "Full screen",
    error: "System does not support full screen"
  },
  notice: {
    title: "Notice center",
    readed: "Mark readed",
  },
  reload: 'Reload',
  personalCenter: 'Personal center',
  logout: {
    index: 'Log out',
    notice: 'Are you sure you want to log out?',
    cancelBtnText: 'No',
  },
  model: {
    title: 'Tips'
  },
  login: {
    text: 'Login',
    changeMode: 'Change login mode',
    username: 'username or phone',
    password: 'password',
    required: 'is required',
    fillIn: 'input',
    register: 'Register',
  },
  user: {
    defaultNickname: 'Visitor',
  }
}

export default en