
export const cachedView = {
  namespaced: true,
  state: {
    cachedView: []
  },
  getters: {
    cachedView(state) {
      return state.cachedView
    }
  },
  mutations: {
    CHANGE_CACHED_VIEW(state, val) {
      state.cachedView = val
    }
  },
  actions: {
    changeCachedView({ commit }, cahcedView) {
      commit('CHANGE_CACHED_VIEW', cahcedView)
    }
  }
}