export const webset = {
  namespaced: true,
  state: {
    lang: 'cn', // 系统语言
    theme: 'light', // 系统模式
    isCollapsed: false, // 左侧菜单栏是否收起
    menuSelected: []
  },
  getters: {
    lang(state) {
      return state.lang
    },
    isCollapsed(state) {
      return state.isCollapsed
    },
    theme(state) {
      return state.theme
    },
    menuSelected(state) {
      return state.menuSelected
    }
  },
  mutations: {
    // 变换菜单收缩状态
    CHANGE_COLLAPSED(state, val = null) {
      state.isCollapsed = val !== null ? val : !state.isCollapsed;
    },
    // 变化菜单选中项
    CHANGE_MENU_SELECTED(state, key) {
      state.menuSelected = [key]
    },
    // 切换语言
    CHANGE_LANG(state, val) {
      state.lang = val
      localStorage.setItem('lang', val)
    }
  },
  actions: {
    changeCollapsed({ commit }, val) {
      commit('CHANGE_COLLAPSED', val)
    },
    changeMenuSelected({ commit }, key) {
      commit('CHANGE_MENU_SELECTED', key)
    },
    changeLang({ commit }, val) {
      commit('CHANGE_LANG', val)
    }
  }
}