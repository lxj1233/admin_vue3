export const visitedView = {
  namespaced: true,
  state: {
    visitedView: []
  },
  getters: {
    visitedView(state) {
      return state.visitedView
    }
  },
  mutations: {
    CHANGE_VISITED_VIEW(state, val) {
      state.visitedView = val
    }
  },
  actions: {
    changeVisitedView({ commit }, val) {
      commit('CHANGE_VISITED_VIEW', val)
    }
  }
}