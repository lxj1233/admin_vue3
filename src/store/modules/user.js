
const userinfo = JSON.parse(localStorage.getItem('user_info') || '{}');

export const user = {
  namespaced: true,
  state: {
    roleId: userinfo.roleId || 0,
    token: userinfo.token || '',
    username: userinfo.username || ''
  },
  getters: {
    userName(state) {
      return state.userName
    },
    isLogin(state) {
      return !!state.token
    }
  },
  mutations: {
    SAVE_USER(state, userinfo) {
      state.roleId = userinfo.roleId
      state.token = userinfo.token
      state.username = userinfo.username
    },
    LOGOUT(state) {
      state.roleId = 0
      state.token = ''
      state.username = ''
      localStorage.clear()
    }
  },
  actions: {
    saveUser({ commit }, userinfo) {
      commit('SAVE_USER', userinfo)
    },
    logout({ commit }) {
      return new Promise((resolve) => {
        commit('LOGOUT')
        resolve()
      })
    }
  }
}