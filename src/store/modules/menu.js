
import { handleRoutes } from '@/unit/router'

export const menu = {
  namespaced: true,
  state: {
    permissionRoutes: []
  },
  getters: {
    isEmptyPermissionRoutes(state) {
      return !state.permissionRoutes || state.permissionRoutes.length === 0
    },
    permissionRoutes(state) {
      return state.permissionRoutes
    }
  },
  mutations: {
    INIT_PERMISSION_ROUTES(state, routes) {
      const tempRoutes = handleRoutes(routes) || []
      state.permissionRoutes.length = 0
      state.permissionRoutes.push(...tempRoutes)
    }
  },
  actions: {
    initPermissionRoutes({ commit }, routes) {
      commit('INIT_PERMISSION_ROUTES', routes)
    }
  }
}