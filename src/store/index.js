import {
  createStore
} from 'vuex'

import { user } from './modules/user'
import { webset } from './modules/webset'
import { menu } from './modules/menu'
import { cachedView } from './modules/cached-view'
import { visitedView } from './modules/visited-view'

const layoutState = {
}

export default createStore({
  state: layoutState,
  getters: {
    cachedViews(state) {
      return state.cachedViews
    }
  },
  mutations: {
  },
  actions: {

  },
  modules: {
    webset: webset,
    user: user,
    menu: menu,
    cachedView: cachedView,
    visitedView: visitedView
  }
})